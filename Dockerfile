ARG BASE_IMAGE_VERSION=6.12.0.122
# container
FROM mono:${BASE_IMAGE_VERSION}

ARG SONAR_SCANNER_VERSION=5.5.3.43281
ARG SONAR_SCANNER_VERSION_SUFFIX=net46

RUN apt-get update -y && \
    apt-get install openjdk-11-jre-headless wget zip -y && \
	mkdir /sonar-scanner && \
	wget -O /sonar-scanner/scanner.zip https://github.com/SonarSource/sonar-scanner-msbuild/releases/download/${SONAR_SCANNER_VERSION}/sonar-scanner-msbuild-${SONAR_SCANNER_VERSION}-${SONAR_SCANNER_VERSION_SUFFIX}.zip && \
	unzip /sonar-scanner/scanner.zip -d /sonar-scanner/ && \
	rm /sonar-scanner/scanner.zip
	
ADD sonar-scanner.sh /usr/bin/sonar-scanner

RUN ["chmod", "+x", "/usr/bin/sonar-scanner"]
RUN ["chmod", "-R", "+x", "/sonar-scanner/"]
